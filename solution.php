#!/usr/bin/php
<?php
ini_set('memory_limit', '-1');

if (empty($argv[1]) || empty($argv[2])) die("Expected 2 arguments\nExample: ./solution.php filename.csv 10\n");

if (!file_exists($file = $argv[1]) || !is_numeric($minimalSum = $argv[2])) die("Incorrect arguments passed\n");

$usersTransactions = prepareData($file);

if (empty($usersTransactions)) die("No users found with amount >= $minimalSum\n");

findUsers($usersTransactions, $minimalSum);

/**
 * Sort transactions by uid, date. Group transactions by uid
 *
 * @param string $file
 * @param bool $skipFirstLine
 * @return array
 */
function prepareData(string $file, bool $skipFirstLine = true) {
    $transactions = [];
    $source = fopen($file, 'r');
    while ($row = fgetcsv($source)) {
        if ($skipFirstLine) {
            $skipFirstLine = false;
            continue;
        }
        if (validateRow($row)) {
            $transactions[] = [
                'user_id' => (int)$row[0],
                'date' => strtotime($row[1]),
                'sum' => (double)$row[2],
            ];
        }
    }

    $usersTransactions = [];
    foreach ($transactions as $transaction) {
        $usersTransactions[$transaction['user_id']][] = $transaction;
    }

    return $usersTransactions;
}

/**
 * Find users by transactions amount
 *
 * <p>File must be a valid comma-separated csv</p>
 *
 * @param array $usersTransactions
 * @param float $minimalSum
 */
function findUsers(array $usersTransactions, float $minimalSum) {
    $result = [];
    foreach ($usersTransactions as $userId => $transactions)
        if ($user = checkUser($transactions, $minimalSum))
            $result[] = $user;

    usort($result, function ($a, $b) {
        return $a['user_id'] <=> $b['user_id'];
    });

    $target = fopen('result.csv', 'w');
    foreach ($result as $row) fputcsv($target, $row);
    fclose($target);

    echo "Found " . count($result) . " users\n";
}

function checkUser(array $userTransactions, float $minimalSum) {
    usort($userTransactions, function ($a, $b) {
        return $a['date'] <=> $b['date'];
    });
    $sum = 0;
    foreach ($userTransactions as $transaction) {
        $sum += $transaction['sum'];
        if ($sum >= $minimalSum)

            return ['user_id' => $transaction['user_id'], 'date' => date("Y-m-d", $transaction['date'])];
    }

    return false;
}

/**
 * Validate row
 *
 * <p>Row must be similar with structure:</p>
 * <p>id,date,sum</p>
 *
 * @param array $row
 * @return bool
 */
function validateRow(array $row) {
    return (int)$row[0] && (bool)strtotime($row[1]) && is_numeric($row[2]);
}
