#!/usr/bin/php
<?php

if (empty($argv[1]) || empty($argv[2])) die("Expected 2 arguments\nExample: ./solution.php filename.csv 10\n");

if (!file_exists($file = $argv[1]) || !is_numeric($minimalSum = $argv[2])) die("Incorrect arguments passed\n");

define('TEMP_TABLE', '_transactions');

$db = new PDO('mysql:host=localhost;dbname=test_db', 'test_db_user', '123123', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

if (uploadTransactions($file))
    findUsers($minimalSum);
else echo "Something went wrong\n";

/**
 * Uploads transactions from csv to database
 *
 * @param string $file
 * @return bool
 */
function uploadTransactions(string $file) {
    global $db;

    $dropTable = "DROP TEMPORARY TABLE IF EXISTS " . TEMP_TABLE;

    $createTable = "
        CREATE TEMPORARY TABLE " . TEMP_TABLE . " 
        (uid INT NOT NULL, date DATE NOT NULL, sum DOUBLE(10,2) NOT NULL)";

    $loadData = "
        LOAD DATA INFILE '$file' INTO TABLE " . TEMP_TABLE . " 
        FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS (uid,date,sum)";

    return $db->query($dropTable) && $db->query($createTable) && $db->query($loadData);
}

/**
 * Find users with total transactions amount >= $minimalSum
 *
 * @param float $minimalSum
 */
function findUsers(float $minimalSum) {
    global $db;

    $result = [];

    $neededUsers = $db->query("
        SELECT uid FROM " . TEMP_TABLE . " 
        GROUP BY uid 
        HAVING SUM(sum) >= $minimalSum 
        ORDER BY uid ASC")
    ->fetchAll(PDO::FETCH_COLUMN);

    foreach ($neededUsers as $neededUser) {
        $usersTransactions = $db->query("
            SELECT uid, date, sum 
            FROM _transactions 
            WHERE uid = $neededUser 
            ORDER BY date ASC")
        ->fetchAll(PDO::FETCH_ASSOC);

        $sum = 0;
        foreach ($usersTransactions as $transaction) {
            $sum += $transaction['sum'];
            if ($sum >= $minimalSum) {
                $result[] = [$neededUser, $transaction['date']];
                break;
            }
        }
    }

    $target = fopen('result2.csv', 'w');
    foreach ($result as $row) fputcsv($target, $row);
    fclose($target);

    echo "Found " . count($result) . " users\n";
}
